<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:m0="http://services.samples" 
	version="2.0"
	xmlns:v1="http://www.firstrand.co.za/gts/opad/OpenPagesService/MaintainContents/v1"
	xmlns:v2="http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2"
	xmlns:v11="http://www.firstrand.co.za/gts/cdm/opad/v1"
	exclude-result-prefixes="m0 fn">
	<xsl:output method="xml" omit-xml-declaration="yes"
		indent="yes" />
	<xsl:template match="*">
		<xsl:element name="v11:{local-name()}">
			<xsl:copy-of select="attribute::*"/>
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>