<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	xmlns:m0="http://services.samples" version="2.0"
	xmlns:v1="http://www.firstrand.co.za/gts/opad/OpenPagesService/RetrieveContentsByID/v1"
	xmlns:v2="http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2"
	exclude-result-prefixes="m0 fn">
	<xsl:output method="xml" omit-xml-declaration="yes"
		indent="yes" />
	<xsl:template match="*">
		<xsl:element name="{local-name()}" namespace="http://www.firstrand.co.za/gts/opad/OpenPagesService/RetrieveContentsByID/v1">
			<xsl:copy-of select="attribute::*"/>
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>