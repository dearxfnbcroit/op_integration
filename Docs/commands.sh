#!/usr/bin/env bash
openssl rsautl -encrypt -inkey crp-riuesbjvm01.fnb.co.za.pem -pubin -in plainusername.txt -out encryptedusername.txt
openssl rsautl -encrypt -inkey crp-riuesbjvm01.fnb.co.za.pem -pubin -in plainusername.txt -out encryptedpassword.txt

base64 encryptedusername.txt > readableusername.txt
base64 encryptedpassword.txt > readablepassword.txt
