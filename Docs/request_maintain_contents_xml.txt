<?xml version='1.0' encoding='utf-8'?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
    xmlns:v1="http://www.firstrand.co.za/gts/opad/OpenPagesService/MaintainContents/v1" 
    xmlns:v2="http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2" 
    xmlns:v11="http://www.firstrand.co.za/gts/cdm/opad/v1">
    <soapenv:Body>
        <v1:MaintainContentsRequest>
            <v2:header>
                <v2:senderID>OPAD</senderID>
                <v2:messageID>d51594b7-67b8-4f81-b7a4-d6f7d9a933e6</v2:messageID>
                <v2:timestamp>2019-07-18T12:04:40.320+02:00</v2:timestamp>
                <v2:correlationID>d51594b7-67b8-4f81-b7a4-d6f7d9a933e6</v2:correlationID>
            </v2:header>
            <v1:contents>
                <v1:name>FNB_FNB Business_Asset Based Finance: PRCIA AU_13. Integrated Performance and Risk Analytics - DEV</v1:name>
                <v11:modifiedDate>2017-07-28T15:46:46.000+02:00</v11:modifiedDate>
                <v1:description>prelim.. Includes day to day operation of Risk Governance Structures and "Management of Governance and performance structures" (may include Audit, Technology etc Structures)

(PC15) Manage Compliance, Legal, Governance and Audit

(PC16) Establish risk management processes and methodologies (apart from standard business process and supervisory controls) to record, monitor, evaluate, control or manage risk exposures within the firm. Includes:
Develop high-level principles or frameworks regarding the acceptance and management of risk
Develop risk-specific regimes for recording, monitoring, evaluation, limits-setting, management, and back-testing of risk exposures to the firm (including credit, market, operational, liquidity, proprietary/business, model and other specified risks)
Establish and maintain insurance and third-party recovery programs

(PC1103) Management Reporting

Excluded:
This category is intended to be narrow; applies to structured risk management, compliance , performance and Legal programs; not to granular management oversight, supervision and process controls.</v1:description>
                <v11:lastModifiedBy>OpenPagesAdministrator</v11:lastModifiedBy>
                <v11:fields>
                    <v11:field>
                        <v11:id>62</v11:id>
                        <v11:dataType>STRING_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>Comment</v11:name>
                    </v11:field>
                    <v11:field>
                        <v11:id>56</v11:id>
                        <v11:dataType>STRING_TYPE</v11:dataType>
                        <v11:name>Description</v11:name>
                        <v11:value>Includes day to day operation of Risk Governance Structures and "Management of Governance and performance structures" (may include Audit, Technology etc Structures)

(PC15) Manage Compliance, Legal, Governance and Audit

(PC16) Establish risk management processes and methodologies (apart from standard business process and supervisory controls) to record, monitor, evaluate, control or manage risk exposures within the firm. Includes:
Develop high-level principles or frameworks regarding the acceptance and management of risk
Develop risk-specific regimes for recording, monitoring, evaluation, limits-setting, management, and back-testing of risk exposures to the firm (including credit, market, operational, liquidity, proprietary/business, model and other specified risks)
Establish and maintain insurance and third-party recovery programs

(PC1103) Management Reporting

Excluded:
This category is intended to be narrow; applies to structured risk management, compliance , performance and Legal programs; not to granular management oversight, supervision and process controls.</v11:value>
                    </v11:field>
                    <v11:field>
                        <v11:id>154</v11:id>
                        <v11:dataType>STRING_TYPE</v11:dataType>
                        <v11:name>FRAdminGroup:Administrator Comments</v11:name>
                        <v11:value>2017-02-21(Assessment round 14 April 2017)</v11:value>
                    </v11:field>
                    <v11:field>
                        <v11:id>198</v11:id>
                        <v11:dataType>STRING_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>OPSS-Process:Additional Description</v11:name>
                    </v11:field>
                    <v11:field>
                        <v11:id>199</v11:id>
                        <v11:dataType>STRING_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>OPSS-Process:Process Owner</v11:name>
                    </v11:field>
                    <v11:field>
                        <v11:id>200</v11:id>
                        <v11:dataType>ENUM_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>FR-Risk Domain:Risk Domain</v11:name>
                    </v11:field>
                    <v11:field>
                        <v11:id>201</v11:id>
                        <v11:dataType>ENUM_TYPE</v11:dataType>
                        <v11:name>FR-Shared-Process:Process Level 1</v11:name>
                        <v11:enumValue>
                            <v11:id>3986</v11:id>
                            <v11:name>10. Integrated Performance and Risk (and compliance) Analytics</v11:name>
                            <v11:localizedLabel>10. Integrated Performance and Risk (and compliance) Analytics</v11:localizedLabel>
                            <v11:index>10</v11:index>
                            <v11:hidden>false</v11:hidden>
                        </v11:enumValue>
                    </v11:field>
                    <v11:field>
                        <v11:id>203</v11:id>
                        <v11:dataType>ENUM_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>FR-Shared-Process:Process Level 2</v11:name>
                    </v11:field>
                    <v11:field>
                        <v11:id>202</v11:id>
                        <v11:dataType>ENUM_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>FR-Shared-Process:Process Rating</v11:name>
                    </v11:field>
                    <v11:field>
                        <v11:id>206</v11:id>
                        <v11:dataType>STRING_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>FR-Process:BA Reason</v11:name>
                    </v11:field>
                    <v11:field>
                        <v11:id>205</v11:id>
                        <v11:dataType>ENUM_TYPE</v11:dataType>
                        <v11:name>FR-Process:Business Applicability</v11:name>
                        <v11:enumValue>
                            <v11:id>3988</v11:id>
                            <v11:name>Yes</v11:name>
                            <v11:localizedLabel>Yes</v11:localizedLabel>
                            <v11:index>1</v11:index>
                            <v11:hidden>false</v11:hidden>
                        </v11:enumValue>
                    </v11:field>
                    <v11:field>
                        <v11:id>204</v11:id>
                        <v11:dataType>ENUM_TYPE</v11:dataType>
                        <v11:hasChanged>false</v11:hasChanged>
                        <v11:name>FR-Process:Business Defined Process Type</v11:name>
                        <v11:enumValue>
                            <v11:id>3990</v11:id>
                            <v11:name>TBD</v11:name>
                            <v11:localizedLabel>TBD</v11:localizedLabel>
                            <v11:index>1</v11:index>
                            <v11:hidden>false</v11:hidden>
                        </v11:enumValue>
                    </v11:field>
                </v11:fields>
                <v1:typeDefinitionId>65</v1:typeDefinitionId>
                <v1:primaryParentId>489958</v1:primaryParentId>
            </v1:contents>
            <v1:maintenanceType>MAINTAIN</v1:maintenanceType>
        </v1:MaintainContentsRequest>
    </soapenv:Body>
</soapenv:Envelope>