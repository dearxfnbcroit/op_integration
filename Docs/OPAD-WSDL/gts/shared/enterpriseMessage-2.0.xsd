<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:tns="http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2"
           targetNamespace="http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2"
           elementFormDefault="qualified" attributeFormDefault="unqualified" version="0">
    <xs:annotation>
        <xs:documentation>
            This is the default enterprise message. Messages published onto the ESB includes this schema.
        </xs:documentation>
    </xs:annotation>
    <xs:complexType name="EnterpriseMessage">
        <xs:annotation>
            <xs:documentation>
                Any message published onto the ESB must extend this type. This adds a header, with the rest of the
                message considered to be the message payload.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="header" type="tns:EnterpriseMessageHeader"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="EnterpriseMessageHeader">
        <xs:sequence>
            <xs:element name="senderID" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Identifier for the system that published the message. The same senderId should be
                        used for all messages produced by a particular system.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="messageID" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Globally unique identifier for this message.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="timestamp" type="xs:dateTime">
                <xs:annotation>
                    <xs:documentation>The time at which this message was sent.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="correlationID" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>
                        Correlates one or more resulting messages to a particular originating message. The
                        correlationdID in the resulting message must be the same as the correlationID in the originating
                        message. The use of this field is domain specific.

                        Example: When a response is expected to a particular request message, both will have the same
                        correlationID.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="originatingMessageID" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>If this message was created in response to another message, that message's eventId
                        must be populated here. Example: While consuming message A, message B is created. Message B's
                        originatingEventID is Message A's eventId.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="referenceID" type="xs:string" maxOccurs="1" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>A reference Identifier for the message. An example would be a financial
                        transaction reference.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="sequence" type="tns:Sequence" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>
                        If message sequencing is required, this field must be populated. Groups of sequenced messages
                        are identified by this field. The value of this field must be globally unique, but the same
                        value must be used for all messages belonging to a particular sequence.

                        Example: If messages A1 and A2 belong to a sequence and messages B1 and B2 belong to another
                        sequence, A1 and A2 must have the same sequenceGroup identifier, and B1 and B2 must have the
                        same sequenceGroup identifier.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="userCredentials" type="tns:UserCredentials" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>If the message is initiated by a user action, this field must be populated with
                        the user ID.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="result" type="tns:ResultType" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>This is a header level reply message that should be used when there's a need to
                        indicate success, fail or reject of a request message.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="messageDescription" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Human readable description of the message.</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="ResultType">
        <xs:sequence>
            <xs:element name="status" type="tns:ResultStatus">
                <xs:annotation>
                    <xs:documentation>Indicates the result of the originating message processing.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="code" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Code of the result. May be populated for success messages as well as failed or
                        rejected messages. In the case of an error, this should match the error code.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="description" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Result message, which should be human readable. In the case of an error, this
                        should be a brief error description, with more detail provided in the description field of the
                        ErrorType
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="severity" type="tns:Severity" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>The severity of an error. This must be populated for any resultCode other than
                        success.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="technicalDescription" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>This is intended for technical users and could include untranslated error messages
                        or stack traces.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="originalCode" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Originating systems may return specific codes that can be useful for the calling
                        system. Examples of this could be a specific status or code.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="originalDescription" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Originating systems may return specific descriptions that can be useful for the
                        calling system. Examples of this could be an error message.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:simpleType name="ResultStatus">
        <xs:restriction base="xs:string">
            <xs:enumeration value="SUCCESS">
                <xs:annotation>
                    <xs:documentation>
                        The originating message was processed successfully
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="FAILURE">
                <xs:annotation>
                    <xs:documentation>
                        The originating message was not processed successfully
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="REJECTED">
                <xs:annotation>
                    <xs:documentation>
                        The originating message was not processed at all
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="UNKNOWN">
                <xs:annotation>
                    <xs:documentation>
                        Used when it is unclear if a message was processed or not. An example of this would be a
                        timeout.
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="Severity">
        <xs:restriction base="xs:string">
            <xs:enumeration value="WARNING">
                <xs:annotation>
                    <xs:documentation>
                        Processing was successful, but abnormal conditions were detected that may require investigation
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="ERROR">
                <xs:annotation>
                    <xs:documentation>
                        Processing was not successful
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="FATAL">
                <xs:annotation>
                    <xs:documentation>
                        Processing was not successful, and caused error conditions that the message originator could not
                        recover from
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="ErrorType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="TECHNICAL">
                <xs:annotation>
                    <xs:documentation>
                        Technical errors include among others, receiving malformed messages, losing connectivity to
                        systems or components etc.
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="BUSINESS">
                <xs:annotation>
                    <xs:documentation>
                        Business errors such posting unbalanced entries or authorization failures etc.
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="Sequence">
        <xs:annotation>
            <xs:documentation>The sequence provides the fields necessary for messages that for an aggregate or
                sequence.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="groupID" type="xs:string">
                <xs:annotation>
                    <xs:documentation>If message sequencing is required, this field must be populated. Groups of
                        sequenced messages are identified by this field. The value of this field must be globally
                        unique, but the same value must be used for all messages belonging to a particular sequence.
                        Example: If messages A1 and A2 belong to a sequence and messages B1 and B2 belong to another
                        sequence, A1 and A2 must have the same sequenceGroup identifier, and B1 and B2 must have the
                        same sequenceGroup identifier.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="number" type="xs:int">
                <xs:annotation>
                    <xs:documentation>The sequence number of a particular message within a sequence group, starting with
                        1. This is used to aggregate and order messages. Other messages within the group must increment
                        this value and have no skipped values up to the sequence size.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="size" type="xs:int" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>The total number of messages in the sequence, if known. This is required for
                        aggregation but not for sequencing.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="UserCredentials">
        <xs:sequence>
            <xs:element name="authenticationSystem" type="xs:string" maxOccurs="1" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>The system used to authenticate the user. An example would be Active Directory.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="userID" type="xs:string" maxOccurs="1" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>The identifier of the user. An example would be an employee F number.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
</xs:schema>