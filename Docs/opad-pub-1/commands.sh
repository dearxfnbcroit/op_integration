#!/usr/bin/env bash
openssl rsautl -encrypt -inkey crp-riuesbjvm01.fnb.co.za.pem -pubin -in plainusername.txt -out encryptedusername.txt
openssl rsautl -encrypt -inkey crp-riuesbjvm01.fnb.co.za.pem -pubin -in plainusername.txt -out encryptedpassword.txt

#cat encryptedusername.txt | openssl rsautl -decrypt -inkey crp-riuesbjvm01.fnb.co.za.pem
#cat encryptedpassword.txt | openssl rsautl -decrypt -inkey crp-riuesbjvm01.fnb.co.za.pem

base64 encryptedusername.txt > readableusername.txt
base64 encryptedpassword.txt > readablepassword.txt
