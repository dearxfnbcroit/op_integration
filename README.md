# OP_Integration

Maintain Contents:
{ "maintainContents":
{
   "senderID": "OPAD",
   "name": "FNB_FNB Business_Asset Based Finance: PRCIA AU_13. Integrated Performance and Risk Analytics - DEV",
   "modifiedDate": "2017-07-28T15:46:46.000+02:00",
   "description": "prelim.. Includes day to day operation of Risk Governance Structures and \"Management of Governance and performance structures\" (may include Audit, Technology etc Structures)\n\n(PC15) Manage Compliance, Legal, Governance and Audit\n\n(PC16) Establish risk management processes and methodologies (apart from standard business process and supervisory controls) to record, monitor, evaluate, control or manage risk exposures within the firm. Includes:\nDevelop high-level principles or frameworks regarding the acceptance and management of risk\nDevelop risk-specific regimes for recording, monitoring, evaluation, limits-setting, management, and back-testing of risk exposures to the firm (including credit, market, operational, liquidity, proprietary/business, model and other specified risks)\nEstablish and maintain insurance and third-party recovery programs\n\n(PC1103) Management Reporting\n\nExcluded:\nThis category is intended to be narrow; applies to structured risk management, compliance , performance and Legal programs; not to granular management oversight, supervision and process controls.",
   "lastModifiedBy": "OpenPagesAdministrator",
   "fields": {"field":    [
            
            {
         "id": "62",
         "dataType": "STRING_TYPE",
         "hasChanged": false,
         "name": "Comment"
      },
           
            {
         "id": "56",
         "dataType": "STRING_TYPE",
         "name": "Description",
         "value": "Includes day to day operation of Risk Governance Structures and \"Management of Governance and performance structures\" (may include Audit, Technology etc Structures)\n\n(PC15) Manage Compliance, Legal, Governance and Audit\n\n(PC16) Establish risk management processes and methodologies (apart from standard business process and supervisory controls) to record, monitor, evaluate, control or manage risk exposures within the firm. Includes:\nDevelop high-level principles or frameworks regarding the acceptance and management of risk\nDevelop risk-specific regimes for recording, monitoring, evaluation, limits-setting, management, and back-testing of risk exposures to the firm (including credit, market, operational, liquidity, proprietary/business, model and other specified risks)\nEstablish and maintain insurance and third-party recovery programs\n\n(PC1103) Management Reporting\n\nExcluded:\nThis category is intended to be narrow; applies to structured risk management, compliance , performance and Legal programs; not to granular management oversight, supervision and process controls."
      },
            {
         "id": "154",
         "dataType": "STRING_TYPE",
         "name": "FRAdminGroup:Administrator Comments",
         "value": "2017-02-21(Assessment round 14 April 2017)"
      },
            {
         "id": "198",
         "dataType": "STRING_TYPE",
         "hasChanged": false,
         "name": "OPSS-Process:Additional Description"
      },
            {
         "id": "199",
         "dataType": "STRING_TYPE",
         "hasChanged": false,
         "name": "OPSS-Process:Process Owner"
      },
            {
         "id": "200",
         "dataType": "ENUM_TYPE",
         "hasChanged": false,
         "name": "FR-Risk Domain:Risk Domain"
      },
            {
         "id": "201",
         "dataType": "ENUM_TYPE",
         "name": "FR-Shared-Process:Process Level 1",
         "enumValue":          {
            "id": "3986",
            "name": "10. Integrated Performance and Risk (and compliance) Analytics",
            "localizedLabel": "10. Integrated Performance and Risk (and compliance) Analytics",
            "index": 10,
            "hidden": false
         }
      },
            {
         "id": "203",
         "dataType": "ENUM_TYPE",
         "hasChanged": false,
         "name": "FR-Shared-Process:Process Level 2"
      },
            {
         "id": "202",
         "dataType": "ENUM_TYPE",
         "hasChanged": false,
         "name": "FR-Shared-Process:Process Rating"
      },
            {
         "id": "206",
         "dataType": "STRING_TYPE",
         "hasChanged": false,
         "name": "FR-Process:BA Reason"
      },
            {
         "id": "205",
         "dataType": "ENUM_TYPE",
         "name": "FR-Process:Business Applicability",
         "enumValue":          {
            "id": "3988",
            "name": "Yes",
            "localizedLabel": "Yes",
            "index": 1,
            "hidden": false
         }
      },
            {
         "id": "204",
         "dataType": "ENUM_TYPE",
         "hasChanged": false,
         "name": "FR-Process:Business Defined Process Type",
         "enumValue":          {
            "id": "3990",
            "name": "TBD",
            "localizedLabel": "TBD",
            "index": 1,
            "hidden": false
         }
      }
   ]},
   "typeDefinitionId": "65",
   "isCheckOut": false,
   "isLocked": false,
   "primaryParentId": "489958"
}
}


<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.firstrand.co.za/gts/opad/OpenPagesService/MaintainContents/v1" xmlns:v11="http://www.firstrand.co.za/gts/cdm/opad/v1" xmlns:v2="http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2">
                        <soapenv:Header/>
                        <soapenv:Body>
                            <v1:MaintainContentsRequest>
                                <v2:header>
                                    <v2:senderID>Futuristic</v2:senderID>
                                    <v2:messageID>BC83F5A049</v2:messageID>
                                    <v2:timestamp>2019-07-15T12:29:24</v2:timestamp>
                                    <v2:correlationID>09FA45E6ADF5414</v2:correlationID>
                                </v2:header>
                                <v1:contents>
                                    <v11:id>489986</v11:id>
                                    <v11:name>TEST-1</v11:name>
                                    <v11:modifiedDate>2017-07-28T15:46:46.000+02:00
									</v11:modifiedDate>
                                    <v11:description>TEST</v11:description>
                                    <v11:lastModifiedBy>F4810317</v11:lastModifiedBy>
                                    <v11:fields>
                                        <v11:field>
                                            <v11:id>62</v11:id>
                                            <v11:dataType>STRING_TYPE</v11:dataType>
                                            <v11:hasChanged>true</v11:hasChanged>
                                            <v11:name>Resource ID</v11:name>
                                            <v11:value>31691</v11:value>
                                        </v11:field>
                                    </v11:fields>
                                    <v11:isCheckOut>false</v11:isCheckOut>
                                    <v11:isLocked>false</v11:isLocked>
                                </v1:contents>
                                <v1:maintenanceType>MAINTAIN</v1:maintenanceType>
                            </v1:MaintainContentsRequest>
                        </soapenv:Body>
                    </soapenv:Envelope>