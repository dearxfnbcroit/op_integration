package za.co.firstrand.op;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.synapse.MessageContext;
import org.apache.synapse.config.Entry;
import org.apache.synapse.mediators.AbstractMediator;
import org.apache.synapse.registry.Registry;

public class RSAPWEncrypt extends AbstractMediator {
	private static final String PUBLIC_PATH = null;

	public boolean mediate(MessageContext context) {
		System.out.println("We about to do some genius stuff!");
		String username = (String) context.getProperty("username");
		String password = (String) context.getProperty("password");
		
		try {
			String fileName = "crp-riuesbjvm01.fnb.co.za.cer";
			ClassLoader classLoader = ClassLoader.getSystemClassLoader();
			File file = new File(classLoader.getResource(fileName).getFile());
			InputStream targetStream = new FileInputStream(file);
	         
	        //File is found
	        System.out.println("File Found : " + file.exists());
			
			final CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			Certificate cert = certFactory.generateCertificate(targetStream);
			PublicKey publicKey = cert.getPublicKey();
			byte[] encryptedUsername = encrypt(publicKey, username);
		    byte[] encryptedPassword = encrypt(publicKey, password);
		    System.out.println("cipher:\n" + Base64.getEncoder().encodeToString(encryptedUsername));
			
			context.setProperty("encryptedUsername", Base64.getEncoder().encodeToString(encryptedUsername));
			context.setProperty("encryptedPassword", Base64.getEncoder().encodeToString(encryptedPassword));

		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}
	
	private static byte[] encrypt(Key pubkey, String text) {
	    try {
	        Cipher rsa;
	        rsa = Cipher.getInstance("RSA");
	        rsa.init(Cipher.ENCRYPT_MODE, pubkey);
	        return rsa.doFinal(text.getBytes());
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	/**
	 * This method will read the pem file which has the public key
	 * 
	 * 
	 * @param privateKey
	 * @param keyPassword
	 * @return
	 * @throws IOException
	 */
	private static Key readPublicKey(File privateKey, String keyPassword) throws IOException {
	    FileReader fileReader = new FileReader(privateKey);
	    PEMReader r = new PEMReader(fileReader, new DefaultPasswordFinder(keyPassword.toCharArray()));
	    try {
	        return (RSAPublicKey) r.readObject();
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        r.close();
	        fileReader.close();
	    }
	}
	
	
	 public static PublicKey getPublicKey(String base64PublicKey){
	        PublicKey publicKey = null;
	        try{
	            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
	            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	            publicKey = keyFactory.generatePublic(keySpec);
	            return publicKey;
	        } catch (NoSuchAlgorithmException e) {
	            e.printStackTrace();
	        } catch (InvalidKeySpecException e) {
	            e.printStackTrace();
	        }
	        return publicKey;
	    }
	 
	 public static byte[] encrypt(String data, String publicKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
	        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
	        return cipher.doFinal(data.getBytes());
	    }

}
